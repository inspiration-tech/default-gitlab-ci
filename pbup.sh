#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd $script_dir

bash ./pbuild.sh
bash ./pup.sh

cd -
