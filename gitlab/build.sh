#!/bin/bash

echo "Building..."

BUILD_ENV="$1" # prod, dev
ARTIFACT_NAME="artifacts/app_$BUILD_ENV.jar"

mkdir artifacts

if [ -f "mvnw" ]; then
    chmod +x ./mvnw
    ./mvnw clean package

    for file in target/*.jar; do
        mv "$file" "$ARTIFACT_NAME"
    done

    chmod +x "$ARTIFACT_NAME"
fi

if [ -f "gradlew" ]; then
    chmod +x ./gradlew
    ./gradlew clean build

    for file in build/libs/*.jar; do
        mv "$file" "$ARTIFACT_NAME"
    done

    chmod +x "$ARTIFACT_NAME"
fi
