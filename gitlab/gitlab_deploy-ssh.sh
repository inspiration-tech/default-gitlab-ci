#!/bin/bash

# script executed on the remote server

echo "Executing remote operations..."

GITLAB_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" # gitlab script directory
WORKING_DIR="$(dirname "$GITLAB_SCRIPT_DIR")" # main script directory (deployment script dir)
BUILD_ENV="$1"
ARTIFACT_NAME="app_$BUILD_ENV.jar"

echo "Build env: $BUILD_ENV"

bash "$WORKING_DIR/_build.sh" $BUILD_ENV
bash "$WORKING_DIR/_restart.sh" $BUILD_ENV "$WORKING_DIR/$ARTIFACT_NAME"
