#!/bin/bash

echo "Deploying..."

GITLAB_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" # gitlab script directory
WORKING_DIR="$(dirname "$GITLAB_SCRIPT_DIR")" # main script directory (deployment script dir)
PARENT_WORKING_DIR="$(dirname "$WORKING_DIR")" # parent (project) script directory

REMOTE_SSH_HOST="$1"
REMOTE_SSH_PORT="$2"
REMOTE_SSH_USER="$3"
REMOTE_SSH_PROJECT_DIR="$4" # directory, which will be used as a destination when copying
REMOTE_SSH_PRIVATE_KEY="$5"
BUILD_ENV="$6" # prod, dev
REMOTE_SCRIPT_NAME=gitlab_deploy-ssh.sh
ARTIFACT_NAME="artifacts/app_$BUILD_ENV.jar"

echo "Build env: $BUILD_ENV"

LOCAL_JAR_PATH=$PARENT_WORKING_DIR/artifacts/*.jar
REMOTE_JAVA_DIR=$REMOTE_SSH_PROJECT_DIR/_$BUILD_ENV/java
REMOTE_JAR_PATH=$REMOTE_JAVA_DIR/app.jar

# getting env vars
source "$WORKING_DIR/compose_default.env"

# copying basic deployment-related files except for "java" directory
rsync -apv --delete \
    --include=".*" \
    --exclude="_dev/compose.env" \
    --exclude="_dev/java/" \
    --exclude="_prod/compose.env" \
    --exclude="_prod/java/" \
    -e "ssh -i $REMOTE_SSH_PRIVATE_KEY -p "$REMOTE_SSH_PORT"" \
    "$WORKING_DIR/" "$REMOTE_SSH_USER@$REMOTE_SSH_HOST:$REMOTE_SSH_PROJECT_DIR"

# copying the jar artifact
rsync -apv \
    -e "ssh -i $REMOTE_SSH_PRIVATE_KEY -p "$REMOTE_SSH_PORT"" \
    "$PARENT_WORKING_DIR/$ARTIFACT_NAME" "$REMOTE_SSH_USER@$REMOTE_SSH_HOST:$REMOTE_SSH_PROJECT_DIR"

# creating static resources directory if not exists, make sh-files executable
ssh -i "$REMOTE_SSH_PRIVATE_KEY" -p "$REMOTE_SSH_PORT" \
    "$REMOTE_SSH_USER@$REMOTE_SSH_HOST" \
    "mkdir -p $REMOTE_JAVA_DIR/$STATIC_RES_DIR && chmod -R u+x "$REMOTE_SSH_PROJECT_DIR/*.sh""

# copying the static resources
rsync -apv \
    -e "ssh -i $REMOTE_SSH_PRIVATE_KEY -p "$REMOTE_SSH_PORT"" \
    "$PARENT_WORKING_DIR/$STATIC_RES_DIR" "$REMOTE_SSH_USER@$REMOTE_SSH_HOST:$REMOTE_JAVA_DIR/$STATIC_RES_DIR"

# executing the deployment script on the remote server
ssh -i "$REMOTE_SSH_PRIVATE_KEY" -p "$REMOTE_SSH_PORT" \
    "$REMOTE_SSH_USER@$REMOTE_SSH_HOST" \
    "bash $REMOTE_SSH_PROJECT_DIR/gitlab/gitlab_deploy-ssh.sh $BUILD_ENV"
