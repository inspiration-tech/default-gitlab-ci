#!/bin/bash

echo "Testing..."

BUILD_ENV="$1" # prod, dev

if [ -f "mvnw" ]; then
    chmod +x ./mvnw
    ./mvnw clean test

    # check the last command exit code
    if [ $? -ne 0 ]; then
        echo "There are failed Maven tests"
        exit 1
    fi
fi

if [ -f "gradlew" ]; then
    chmod +x ./gradlew
    ./gradlew clean test

    # check the last command exit code
    if [ $? -ne 0 ]; then
        echo "There are failed Gradle tests"
        exit 1
    fi
fi
