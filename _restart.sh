#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source $script_dir/_init.sh $1

cd "$FULL_TARGET_DIR"
eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME stop java"
cd -

if [ -n "$2" ]; then
    mv "$2" "$TARGET_JAR"
    chmod +x "$TARGET_JAR"
fi

cd "$FULL_TARGET_DIR"
eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME up -d java"
cd -
