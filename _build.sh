#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source $script_dir/_init.sh $1

if [ -f "../mvnw" ]; then
    cd ..
    ./mvnw clean package
    cp target/*.jar "$TARGET_JAR"
    chmod +x "$TARGET_JAR"
    mkdir -p "$STATIC_FULL_DIR"
    rsync -apv --delete --include=".*" "$STATIC_RES_DIR" "$STATIC_FULL_DIR"
    cd -
fi

if [ -f "../gradlew" ]; then
    cd ..
    ./gradlew clean build
    cp build/libs/*.jar "$TARGET_JAR"
    chmod +x "$TARGET_JAR"
    mkdir -p "$STATIC_FULL_DIR"
    rsync -apv --delete --include=".*" "$STATIC_RES_DIR" "$STATIC_FULL_DIR"
    cd -
fi

cd "$FULL_TARGET_DIR"
eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME build"
cd -
